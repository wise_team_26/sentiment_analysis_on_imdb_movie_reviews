# Sentiment Analysis on IMDB Movie Reviews
Description:

So, what exactly is sentiment? Sentiment relates to the meaning of a word or sequence of words and is usually associated with an opinion or emotion. And analysis? Well, this is the process of looking at data and making inferences; in this case, using machine learning to learn and predict whether a movie review is positive or negative.

Problem Statement:

Devise a Sentiment analysis model on large sets of movie reviews and to apply the model in real time reviews from IMDB.
